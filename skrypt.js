var dane = {
    sekundy: 60,
    punkty: 0
}
window.onload=odliczanie;
var odliczanieTimeout;
function odliczanie(){
    console.log("Sekundy: "+dane.sekundy);
        $("#time").text(dane.sekundy);
        dane.sekundy--;
        odliczanieTimeout = setTimeout("odliczanie()",1000);
    if(dane.sekundy==0){
        $(".circle").css({"display":"none"});
        $(".icircle").css({"display":"none"});
        $(".circle2").css({"display":"none"});
        clearTimeout(odliczanieTimeout);
        $(".gameEnd").css({"display":"block"});
        $("#finalPoints").text(dane.punkty);
        $("#newGameButton").click(function(){
            location.reload();
            $(".gameEnd").css({"display":"none"});
        });
    }
    if(dane.sekundy<10)
    {
       $("#time").css({"color" : "red"});
    }
}
$(document).ready(function(){
    $(document).mousemove(function(strona){ 
        console.log("Pozycja Kursora: X: " + strona.pageX + ", Y: " + strona.pageY);
        $(".circle").css({"margin-top": strona.pageY-85 + "px", "margin-left": strona.pageX-15 + "px" });
    });
    $(".circle2").click(function(){
        dane.punkty++;
        $("#points").text(dane.punkty);
        $(".circle2").css({"margin-left" : Math.floor((Math.random() * 1500) + 1)});
        $(".circle2").css({"margin-top" : Math.floor((Math.random() * 900) + 1)});
    });
});
$(document).ready(function(){
    $("#info").click(function(){
        $(".gameInfo").css({"display":"block"});
        $(".gameEnd").css({"display":"none"});
        $(".circle2").css({"display":"none"});
        clearTimeout(odliczanieTimeout);
    });
    $("#closeInfoButton").click(function(){
        $(".gameInfo").css({"display":"none"});
        location.reload();
    });
});
$(document).ready(function(){
    $(".ulContainer").mouseover(function(){
        $(".circle").css({"display":"none"});
        $(".icircle").css({"display":"none"});
    });
    $(".ulContainer").mouseout(function(){
        $(".circle").css({"display":"none"});
        $(".icircle").css({"display":"none"});
    });
});
